
<?php

  // Create connection
  $conn = mysqli_connect("localhost", "root", "", "help2");

  $sql = "SELECT * FROM data ORDER BY id DESC";
  $result = mysqli_query($conn, $sql) or die("Connection failed");

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="./css/bootstrap.min.css">
    <link rel="stylesheet" href="./css/custom.css">
</head>
<body>

<div class="row">
    <div class="col-md-2"></div>
      <div class="text-center mb-5 col-md-8">
        <a style='text-decoration:none' class=" mt-5 mb-5 "> <button class="btn btn-success mt-4">Received Response</button></a>
          <table class="table  table-bordered table-striped mt-2 table-hover">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col">Mobile</th>
                <th scope="col">Email</th>
                <th scope="col">Opinion</th>
                <th scope="col">Delete</th>
              </tr>
            </thead>
              <tbody>
                      <?php
                        $i = 1;
                        if (mysqli_num_rows($result) > 0) {
                            // output data of each row
                            while($row = mysqli_fetch_assoc($result))  { ?>
                                <tr>
                                    <td><?= $i++ ?></td>
                                    <td><?= $row['name'] ?></td>
                                    <td><?= $row['mobile'] ?></td>
                                    <td><?= $row['email'] ?></td>
                                    <td><?= $row['opinion'] ?></td>
                                    <td><a href="delete.php?id=<?php echo $row['id']; ?>">Delete</a></td>
                                </tr>
                    <?php } } ;?>
                    
              </tbody>
          </table>
      </div>
</div>

</body>
</html>